import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse,
    HttpResponse,
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { mergeMap, catchError } from 'rxjs/operators';
import { environment } from './../../environments/environment';

import { TokenService, SettingsService } from '../services';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class DefaultInterceptor implements HttpInterceptor {
    constructor(
        private router: Router,
        private token: TokenService,
        private settings: SettingsService,
        private snackBar: MatSnackBar
        // private startup: StartupService,
    ) { }


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Add server host
        const url = environment.SERVER_ORIGIN + req.url;
        // Only intercept API url
        if (!url.includes('/api/')) {
            return next.handle(req);
        }
        // All APIs need JWT authorization
        const headers = {
            'Accept': 'application/json',
            'Accept-Language': 'en-US',
            'Authorization': `Bearer ${this.token.get().token}`,
        };


        if (!url.includes('/api/auth')) {
            const newReq = req.clone({ url, setHeaders: headers });
            // return next.handle(newReq);
            return next.handle(newReq).pipe(
                mergeMap((event: HttpEvent<any>) => this.handleOkReq(event)),
                catchError((error: HttpErrorResponse) => this.handleErrorReq(error))
            );
        } else {
            const newReq = req.clone({ url });
            // return next.handle(newReq);
            return next.handle(newReq).pipe(
                mergeMap((event: HttpEvent<any>) => this.handleOkReq(event)),
                catchError((error: HttpErrorResponse) => this.handleErrorReq(error))
            );
        }
    }

    private goto(url: string) {
        setTimeout(() => this.router.navigateByUrl(url));
    }

    private handleOkReq(event: HttpEvent<any>): Observable<any> {
        if (event instanceof HttpResponse) {
            if (event.status === 200 && event.url.includes('/api/auth/signin')) {
                const { token, uid, username } = { token: event.body.accessToken, uid: event.body.id, username: event.body.phoneNumber };
                // Set user info
                this.settings.setUser({
                    id: uid,
                    name: event.body.name,
                    email: event.body.email
                });

                // Set token info
                this.token.set({ token, uid, username });
            } else {
                return of(event);
            }
        }
        // Pass down event if everything is OK
        return of(event);
    }

    private handleErrorReq(error: HttpErrorResponse): Observable<never> {
        switch (error.status) {
            case 400:
                this.snackBar.open(error.error.message, '', { duration: 2500 });
                break;
            case 401:
                this.snackBar.open(error.error.error || `${error.status} ${error.statusText}`, '', { duration: 2500 });
                this.goto(`/auth/login`);
                break;
            case 403:
            case 404:
            case 500:
                this.goto(`/sessions/${error.status}`);
                break;
            default:
                if (error instanceof HttpErrorResponse) {
                    console.error('ERROR', error);
                    this.snackBar.open(error.error || `${error.status} ${error.statusText}`, '', { duration: 2500 });
                }
                break;
        }
        return throwError(error);
    }
}
