import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { environment } from '../environments/environment';
import { AdminLayoutComponent } from './components/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './components/auth-layout/auth-layout.component';
import { LoginComponent } from './components/core/login/login.component';
import { AccountsComponent } from './components/core/accounts/accounts.component';
import { DepositComponent } from './components/core/deposit/deposit.component';
import { WithdrawalComponent } from './components/core/withdrawal/withdrawal.component';
import { AuthGuardService } from './services';
import { EmiCalculatorComponent } from './components/core/emi-calculator/emi-calculator.component';

const routes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuardService],
    canActivateChild: [AuthGuardService],
    children: [
      { path: '', redirectTo: 'accounts', pathMatch: 'full' },
      {
        path: 'accounts',
        component: AccountsComponent,
      },
      {
        path: 'emi-calculator',
        component: EmiCalculatorComponent,
      }
    ],
  },
  {
    path: 'auth',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'login', component: LoginComponent,
        data: { title: 'Login', titleI18n: 'login' }
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: environment.useHash,
  })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
