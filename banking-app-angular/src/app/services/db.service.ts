import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class DbService {

  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }
  private headers = new HttpHeaders().set('Content-Type', 'application/json');

  doGet(url, successCallBack, errorCallBack) {
    this.http.get(url, { headers: this.headers }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });
  }

  doGetWithResponse(url, successCallBack, errorCallBack) {
    this.http.get(this.baseUrl + url, { headers: this.headers, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });
  }

  doGetWithParams(url, params, successCallBack, errorCallBack) {
    this.http.get(url, { params: params, headers: this.headers }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });
  }

  doPostWithParams(url, params, successCallBack, errorCallBack) {
    this.http.post(url, { params: params, headers: this.headers }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });
  }

  doGetWithParamsAndResponse(url, params, successCallBack, errorCallBack) {
    this.http.get(this.baseUrl + url, { params: params, headers: this.headers, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });
  }

  doGetObservable(url): Observable<any> {
    return this.http.get(url, { headers: this.headers });
  }

  doPost(url, data, successCallBack, errorCallBack) {
    this.http.post(url, data, { headers: this.headers }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }

  //Post with response
  doPostWithResponse(url, data, successCallBack, errorCallBack) {
    this.http.post(this.baseUrl + url, JSON.stringify(data), { headers: this.headers, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }

  doPostWithParamsAndResponse(url, data, params, successCallBack, errorCallBack) {
    this.http.post(url, JSON.stringify(data), { params: params, headers: this.headers, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }

  doPostWithParamsAndResponseWithoudBody(url, params, successCallBack, errorCallBack) {
    this.http.post(url, { params: params, headers: this.headers, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }

  //Post file
  doPostFile(url, data, successCallBack, errorCallBack) {
    this.http.post(url, data).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }

  //Put
  doPut(url, data, successCallBack, errorCallBack) {
    this.http.put(url, JSON.stringify(data), { headers: this.headers }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }

  //Put with response
  doPutWithResponse(url, data, successCallBack, errorCallBack) {
    this.http.put(url, JSON.stringify(data), { headers: this.headers, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }

  //Put with params
  doPutWithParams(url, data, params, successCallBack, errorCallBack) {
    this.http.put(url, JSON.stringify(data), { params: params, headers: this.headers }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });
  }

  //Put with params and response
  doPutWithParamsAndResponse(url, data, params, successCallBack, errorCallBack) {
    this.http.put(url, JSON.stringify(data), { params: params, headers: this.headers, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });
  }

  doDelete(url, successCallBack, errorCallBack) {
    this.http.delete(url, { headers: this.headers, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }

  doDeleteWithParamsAndResponse(url, params, successCallBack, errorCallBack) {
    this.http.delete(url, { params: params, headers: this.headers, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }

  doPostForJobs(url, successCallBack, errorCallBack) {
    this.http.post(url, { headers: this.headers }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }

  doPostWithEmailHead(url, data, cleintCode, successCallBack, errorCallBack) {
    this.http.post(url, JSON.stringify(data), { headers: { 'Content-Type': 'application/json', 'Client_Code': cleintCode }, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }

  doGetWithEmailHead(url, cleintCode, successCallBack, errorCallBack) {
    this.http.get(url, { headers: { 'Content-Type': 'application/json', 'Client_Code': cleintCode }, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });
  }

  doPutWithEmailHead(url, cleintCode, successCallBack, errorCallBack) {
    this.http.put(url, '', { headers: { 'Content-Type': 'application/json', 'Client_Code': cleintCode }, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });
  }

  doPutWithEmailPwdHead(url, data, cleintCode, successCallBack, errorCallBack) {
    this.http.put(url, JSON.stringify(data), { headers: { 'Content-Type': 'application/json', 'Client_Code': cleintCode }, observe: 'response' }).subscribe(response => {
      successCallBack(response);
    }, error => {
      errorCallBack(error);
    });

  }


}
