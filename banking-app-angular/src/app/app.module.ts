import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminLayoutComponent } from './components/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './components/auth-layout/auth-layout.component';
import { LoginComponent } from './components/core/login/login.component';
import { httpInterceptorProviders } from './interceptors';
import { HeaderComponent } from './components/core/header/header.component';
import { AccountsComponent } from './components/core/accounts/accounts.component';
import { DepositComponent } from './components/core/deposit/deposit.component';
import { WithdrawalComponent } from './components/core/withdrawal/withdrawal.component';
import { NewAccountComponent } from './components/core/new-account/new-account.component';
import { TransactionsComponent } from './components/core/transactions/transactions.component';
import { ProfileCardComponent } from './components/core/profile-card/profile-card.component';
import { EmiCalculatorComponent } from './components/core/emi-calculator/emi-calculator.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    LoginComponent,
    HeaderComponent,
    AccountsComponent,
    DepositComponent,
    WithdrawalComponent,
    NewAccountComponent,
    TransactionsComponent,
    ProfileCardComponent,
    EmiCalculatorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
