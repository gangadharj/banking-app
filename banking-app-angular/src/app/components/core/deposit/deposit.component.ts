import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DbService } from '../../../services';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.scss']
})
export class DepositComponent implements OnInit {

  depositForm: FormGroup;

  constructor(
    private dbService: DbService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<DepositComponent>,
    @Inject(MAT_DIALOG_DATA) public accountId) {
    this.depositForm = this.fb.group({
      ammount: ['', [Validators.required]],
      description: ['', [Validators.required]],
      reference: ['']
    });
  }

  ngOnInit(): void {
  }

  onClickCancel(): void {
    this.dialogRef.close();
  }

  only_numbers(event) {
    var k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return k >= 48 && k <= 57;
  }

  discard_special_char(event) {
    var k;
    k = event.charCode;
    return (k > 64 && k < 91) || (k > 96 && k < 123) || (k >= 48 && k <= 57);
  }

  deposit() {
    if (this.depositForm.get('ammount').value > 99) {
      let obj = {
        description: this.depositForm.get('description').value,
        reference: this.depositForm.get('reference').value,
        deposit: this.depositForm.get('ammount').value,
        account: {
          id: this.accountId
        }
      }
      this.dbService.doPostWithResponse('/add-transaction', obj, success => {
        if (success.status === 201) {
          this.dialogRef.close();
          this.snackBar.open('Deposited successfully', '', { duration: 2500 });
        }
      }, error => {
        console.log(error);
      });
    } else {
      this.snackBar.open('Deposit ammount should be greater than 99', '', { duration: 2500 });
    }

  }
}

