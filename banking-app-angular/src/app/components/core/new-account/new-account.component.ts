import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.scss']
})
export class NewAccountComponent implements OnInit {

  accountForm: FormGroup;
  accountTypes = [{ value: 'Saving Account' }, { value: 'Regular Savings' }, { value: 'DEMAT Account' }, { value: 'Fixed Deposit Account' }]
  branchCodes = [{ value: 'SBIN0012344' }, { value: 'SBIN00123431' }, { value: 'SBIN00123447' }, { value: 'SBIN0012345' }]

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<NewAccountComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    this.accountForm = this.fb.group({
      bankName: ['', [Validators.required]],
      branch: ['', [Validators.required]],
      accountHolderName: ['', [Validators.required]],
      accountNumber: ['', [Validators.required]],
      accountType: ['', [Validators.required]],
      bankCode: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
  }

  onClickCancel(): void {
    this.dialogRef.close();
  }

  only_numbers(event) {
    var k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return k >= 48 && k <= 57;
  }

  discard_special_char(event) {
    var k;
    k = event.charCode;
    return (k > 64 && k < 91) || (k > 96 && k < 123) || (k >= 48 && k <= 57);
  }

  createAccout() {
    this.dialogRef.close(this.accountForm.value);
  }
}
