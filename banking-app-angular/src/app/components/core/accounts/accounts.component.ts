import { Component, OnInit } from '@angular/core';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { NewAccountComponent } from '../new-account/new-account.component';
import { MatDialog } from "@angular/material/dialog";
import { DbService } from '../../../services';
import { DepositComponent } from '../deposit/deposit.component';
import { WithdrawalComponent } from '../withdrawal/withdrawal.component';
import { TransactionsComponent } from '../transactions/transactions.component';
import { ProfileCardComponent } from '../profile-card/profile-card.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  accounts = [];
  searchSubject$ = new Subject<string>();

  constructor(
    public dialog: MatDialog,
    private dbService: DbService,
    private snackBar: MatSnackBar
  ) {
    this.searchSubject$.pipe(debounceTime(300), distinctUntilChanged()).subscribe(searchKey => {
      this.getAccounts(searchKey);
    });
  }

  ngOnInit(): void {
    this.getAccounts('');
  }

  getAccounts(searchKey) {
    this.dbService.doGetWithParamsAndResponse('/get-all-accounts', { searchKey: searchKey }, success => {
      if (success.status === 200) {
        this.accounts = success.body;
      } else {
        this.accounts = [];
      }
    }, error => {
      console.log(error);
    });
  }

  newAccount() {
    let dialogRef = this.dialog.open(NewAccountComponent, {
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dbService.doPostWithResponse('/add-account', result, success => {
          if (success.status === 201) {
            this.getAccounts('');
            this.snackBar.open('Account Created successfully!', '', { duration: 2500 });
          }
        }, error => {
          console.log(error);
        });
      }
    });
  }

  deposit(id) {
    let dialogRef = this.dialog.open(DepositComponent, {
      disableClose: true,
      width: '300px',
      data: id
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  withdraw(id) {
    let dialogRef = this.dialog.open(WithdrawalComponent, {
      disableClose: true,
      width: '300px',
      data: id
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  transaction(id) {
    this.dbService.doGetWithParamsAndResponse('/get-transactions-by-account-id', { id: id }, success => {
      if (success.status === 200) {
        let dialogRef = this.dialog.open(TransactionsComponent, {
          disableClose: false,
          width: '800px',
          data: success.body
        });
        dialogRef.afterClosed().subscribe(result => {
        });
      } else if (success.status === 204) {
        this.snackBar.open('No transactions found!', '', { duration: 3500 });
      }
    }, error => {
      console.log(error);
    });


  }

  balance(id) {
    this.dbService.doGetWithParamsAndResponse('/get-balance-by-account-id', { id: id }, success => {
      if (success.status === 200) {
        this.snackBar.open('Available bank balance is : '+success.body.message, '', { duration: 3500 });
      }
    }, error => {
      console.log(error);
    });
  }

  profileView(data) {
    let dialogRef = this.dialog.open(ProfileCardComponent, {
      disableClose: false,
      data: data,
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
