import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthGuardService } from './../../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authGuard: AuthGuardService,
  ) {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  ngOnInit() { }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  login() {
    const reqData = {
      usernameOrEmail: this.loginForm.get('username').value,
      password: this.loginForm.get('password').value
    };
    this.authGuard.doLogin(reqData, success => {
      if (success != null) {
        this.router.navigateByUrl('/accounts');
      }
    }, error => {
      console.log(error);
    });
  }

}
