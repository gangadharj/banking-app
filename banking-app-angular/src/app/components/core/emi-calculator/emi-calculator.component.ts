import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-emi-calculator',
  templateUrl: './emi-calculator.component.html',
  styleUrls: ['./emi-calculator.component.scss']
})
export class EmiCalculatorComponent implements OnInit {

  public loanAmount: number;
  public loanAmtMax: number;

  public intRate: number;
  public intRateMax: number;

  public loanDuration: number;
  public loanDurMax: number;

  public emi: number;
  public totalPayment: number;
  public totalInterest: number;

  public slider: any;

  loanForm: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) {
    this.loanForm = this.fb.group({
      ammount: ['', [Validators.required]],
      interest: ['', [Validators.required]],
      duration: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.loanAmtMax = 1500000;
    this.intRateMax = 100;
    this.loanDurMax = 36;
    this.reset();
  }

  only_numbers(event) {
    var k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return k >= 48 && k <= 57;
  }


  reset() {
    this.emi = 0;
    this.totalPayment = 0;
    this.totalInterest = 0;
  }

  Emit(e) {
    this.intRate = e.value;
  }

  updateSliderMax(code) {
    switch (code) {
      case 'LoanAmount':
        (this.loanForm.get('ammount').value > this.loanAmtMax) ? this.loanAmtMax = this.loanForm.get('ammount').value : '';
        this.calculateEMI();
        break;
      case 'InterestRate':
        (this.loanForm.get('interest').value > this.intRateMax) ? this.intRateMax = this.loanForm.get('interest').value : '';
        this.calculateEMI();
        break;
      case 'LoanDuration':
        (this.loanForm.get('duration').value > this.loanDurMax) ? this.loanDurMax = this.loanForm.get('duration').value : '';
        this.calculateEMI();
        break;
      default:
        break;
    }
  }

  calculateEMI() {
    if (this.loanForm.get('ammount').value && this.loanForm.get('interest').value && this.loanForm.get('duration').value) {
      let mir = (this.loanForm.get('interest').value / 100) / 12;
      let emi = this.loanForm.get('ammount').value * mir * Math.pow((1 + mir), this.loanForm.get('duration').value) / ((Math.pow((1 + mir), this.loanForm.get('duration').value)) - 1);
      let totalPayment = emi * this.loanForm.get('duration').value;
      let totalInterest = totalPayment - this.loanForm.get('ammount').value;
      this.emi = Math.round(emi);
      this.totalPayment = Math.round(totalPayment);
      this.totalInterest = Math.round(totalInterest);
    } else {
      this.reset();
    }
  }

}
