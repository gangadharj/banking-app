import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DbService } from '../../../services';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.scss']
})
export class ProfileCardComponent implements OnInit {


  profileData = null;
  availableBalance = null;

  constructor(
    private dbService: DbService,
    public dialogRef: MatDialogRef<ProfileCardComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    this.profileData = data;
  }

  ngOnInit(): void {
    this.balance();
  }

  balance() {
    this.dbService.doGetWithParamsAndResponse('/get-balance-by-account-id', { id: this.profileData.id }, success => {
      if (success.status === 200) {
        this.availableBalance = success.body.message;
      }
    }, error => {
      console.log(error);
    });
  }

}
