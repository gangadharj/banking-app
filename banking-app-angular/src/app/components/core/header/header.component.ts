import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService, TokenService } from '../../../services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private router: Router,
    private settings: SettingsService,
    private token: TokenService,
  ) { }

  ngOnInit(): void {
  }

  logout() {
    this.token.clear();
    this.settings.removeUser();
    this.router.navigateByUrl('/auth/login');
  }

  accounts() {
    this.router.navigateByUrl('/accounts');
  }

  emiCalculator() {
    this.router.navigateByUrl('/emi-calculator');
  }

}
