import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DbService } from '../../../services';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-withdrawal',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.scss']
})
export class WithdrawalComponent implements OnInit {

  withdrawalForm: FormGroup;

  constructor(
    private dbService: DbService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<WithdrawalComponent>,
    @Inject(MAT_DIALOG_DATA) public accountId) {
    this.withdrawalForm = this.fb.group({
      ammount: ['', [Validators.required]],
      description: ['', [Validators.required]],
      reference: ['']
    });
  }

  ngOnInit(): void {
  }

  onClickCancel(): void {
    this.dialogRef.close();
  }

  only_numbers(event) {
    var k;
    k = event.charCode; //         k = event.keyCode;  (Both can be used)
    return k >= 48 && k <= 57;
  }

  discard_special_char(event) {
    var k;
    k = event.charCode;
    return (k > 64 && k < 91) || (k > 96 && k < 123) || (k >= 48 && k <= 57);
  }

  withdraw() {
    if (this.withdrawalForm.get('ammount').value > 99) {
      let obj = {
        description: this.withdrawalForm.get('description').value,
        reference: this.withdrawalForm.get('reference').value,
        withdrawal: this.withdrawalForm.get('ammount').value,
        account: {
          id: this.accountId
        }
      }
      this.dbService.doPostWithResponse('/add-transaction', obj, success => {
        if (success.status === 201) {
          this.dialogRef.close();
          this.snackBar.open('Withdrawal successfully', '', { duration: 2500 });
        }
      }, error => {
        console.log(error);
      });
    } else {
      this.snackBar.open('Withdrawal ammount should be greater than 99', '', { duration: 2500 });
    }

  }
}

