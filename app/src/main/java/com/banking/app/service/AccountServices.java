package com.banking.app.service;

import com.banking.app.dto.Account;

import java.util.List;

public interface AccountServices {

    Account addAccount(Account account);

    List<Account> getAllAccounts(String searchKey);
}
