package com.banking.app.service;

import com.banking.app.dto.User;

import java.util.List;
import java.util.Optional;

public interface UserServices {

    Optional<User> findByEmail(String email);

    Optional<User> findByUsernameOrEmail(String username, String email);

    List<User> findByIdIn(List<Long> userIds);

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    User saveUser(User user);
}
