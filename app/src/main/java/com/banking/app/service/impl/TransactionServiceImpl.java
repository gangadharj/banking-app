package com.banking.app.service.impl;

import com.banking.app.dto.Transaction;
import com.banking.app.repository.TransactionDao;
import com.banking.app.service.TransactionServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionServices {

    @Autowired
    private TransactionDao transactionDao;

    @Override
    public Transaction addTransaction(Transaction transaction) {
        transaction.setTransactionDate(LocalDateTime.now());
        return transactionDao.save(transaction);
    }

    @Override
    public Long getBalanceByAccountId(Long accountId) {
        return transactionDao.getBalance(accountId);
    }

    @Override
    public List<Transaction> getAllTransactionsByAccountId(Long accountId) {
        return transactionDao.getTransactions(accountId);
    }
}
