package com.banking.app.service.impl;

import com.banking.app.dto.Account;
import com.banking.app.dto.User;
import com.banking.app.repository.AccountDao;
import com.banking.app.service.AccountServices;
import com.banking.app.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountServices {

    @Autowired
    private AccountDao accountDao;

    @Autowired
    private UserServices userServices;

    @Override
    public Account addAccount(Account account) {
        User user = new User();
        user.setEmail(account.getAccountHolderName() + "@gmail.com");
        user.setPassword("$2a$10$3wlSCoJ4HYcpZQxl2rjZAu9F3O6oPbcD7lep.9jKeST3Xs/ADzcau");
        user.setName(account.getAccountHolderName());
        user.setRole("Customer");
        user.setUsername(account.getAccountHolderName());
        account.setUserAccount(userServices.saveUser(user));
        return accountDao.save(account);
    }

    @Override
    public List<Account> getAllAccounts(String searchKey) {
        return accountDao.getAllAccounts(searchKey);
    }
}
