package com.banking.app.service;

import com.banking.app.dto.Transaction;

import java.util.List;

public interface TransactionServices {

    Transaction addTransaction(Transaction transaction);

    Long getBalanceByAccountId(Long accountId);

    List<Transaction> getAllTransactionsByAccountId(Long accountId);
}
