package com.banking.app.repository;

import com.banking.app.dto.Account;
import com.banking.app.dto.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Repository
public interface TransactionDao extends JpaRepository<Transaction, Long> {

    @Query(nativeQuery = true, value = "SELECT t.balance FROM transaction t WHERE t.account_id =:accountId ORDER BY t.transaction_date DESC LIMIT 1")
    Long getBalance(@Param("accountId") Long accountId);

    @Query("select t from Transaction t where t.account.id =:accountId order by t.transactionDate DESC")
    List<Transaction> getTransactions(@Param("accountId") Long accountId);
}
