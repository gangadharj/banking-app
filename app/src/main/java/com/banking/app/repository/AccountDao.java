package com.banking.app.repository;

import com.banking.app.dto.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountDao extends JpaRepository<Account, Long> {

    @Query("select a from Account a where (:searchKey is null or :searchKey='' or a.accountNumber LIKE :searchKey or a.userAccount.name LIKE :searchKey)")
    List<Account> getAllAccounts(@Param("searchKey") String searchKey);
}
