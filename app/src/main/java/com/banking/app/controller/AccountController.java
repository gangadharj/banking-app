package com.banking.app.controller;

import com.banking.app.dto.Account;
import com.banking.app.dto.Transaction;
import com.banking.app.payloads.ApiResponse;
import com.banking.app.service.AccountServices;
import com.banking.app.service.TransactionServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class AccountController {

    @Autowired
    private AccountServices accountServices;

    @Autowired
    private TransactionServices transactionServices;

    @PostMapping("/add-account")
    public ResponseEntity authenticateUser(@Valid @RequestBody Account account) {
        Account account1 = accountServices.addAccount(account);
        if (account1 != null) {
            return new ResponseEntity(new ApiResponse(true, "Account created successfully!"), HttpStatus.CREATED);
        } else {
            return new ResponseEntity(new ApiResponse(false, "Invalid account details!"), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/get-all-accounts")
    public ResponseEntity getAllAccounts(@RequestParam(value = "searchKey", required = false) String searchKey) {
        if (searchKey != null) {
            searchKey = "%" + searchKey + "%";
        }
        List<Account> accountList = accountServices.getAllAccounts(searchKey);
        if (!accountList.isEmpty()) {
            return ResponseEntity.ok(accountList);
        } else {
            return new ResponseEntity(new ApiResponse(false, "No data found"), HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/add-transaction")
    public ResponseEntity addTransaction(@Valid @RequestBody Transaction transaction) {

        Long balance = transactionServices.getBalanceByAccountId(transaction.getAccount().getId());
        if (transaction.getWithdrawal() != null) {
            if (balance == null) {
                return new ResponseEntity(new ApiResponse(false, "Please do initial deposit!"), HttpStatus.BAD_REQUEST);
            } else {
                if (transaction.getWithdrawal() > balance) {
                    return new ResponseEntity(new ApiResponse(false, "Insufficient Balance!"), HttpStatus.BAD_REQUEST);
                } else {
                    transaction.setBalance(balance - transaction.getWithdrawal());
                }
            }
        } else {
            if (balance == null) {
                transaction.setBalance(transaction.getDeposit());
            } else {
                transaction.setBalance(balance + transaction.getDeposit());
            }

        }
        Transaction transaction1 = transactionServices.addTransaction(transaction);
        if (transaction1 != null) {
            if (transaction1.getDeposit() != null) {
                return new ResponseEntity(new ApiResponse(true, "Deposited successfully!"), HttpStatus.CREATED);
            } else {
                return new ResponseEntity(new ApiResponse(true, "Withdrawal successfully!"), HttpStatus.CREATED);
            }
        } else {
            return new ResponseEntity(new ApiResponse(false, "Invalid transaction details!"), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/get-transactions-by-account-id")
    public ResponseEntity getTransactions(@RequestParam("id") Long id) {
        List<Transaction> transactionList = transactionServices.getAllTransactionsByAccountId(id);
        if (!transactionList.isEmpty()) {
            return ResponseEntity.ok(transactionList);
        } else {
            return new ResponseEntity(new ApiResponse(false, "No data found"), HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/get-balance-by-account-id")
    public ResponseEntity getBalance(@RequestParam("id") Long id) {
        Long balance = transactionServices.getBalanceByAccountId(id);
        if (balance != null) {
            return new ResponseEntity(new ApiResponse(true, "" + balance), HttpStatus.OK);
        } else {
            return new ResponseEntity(new ApiResponse(true, "0"), HttpStatus.OK);
        }
    }

}
